package com.example.demo;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HttpControllerREST extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	public String drawStyle() {
		return
				"<style>" +
				"ul.menu>li{display:inline-block;padding:10px;}" +
				"</style>";
	}
	
	public String drawMenu() {
		return
				"<div>" +
				"<ul class='menu'> " +
				"<li><a href='/'>Start Page</a></li>" +
				"<li><a href='/about'>About</a></li>" +
				"<li><a href='/news'>News</a></li>" +
				"<li><a href='/form'>Form</a></li>" +
				"</ul>" +
				"</div>";
	}
	
	@RequestMapping("/")
	public String index(HttpServletRequest request, HttpServletResponse response) {
		String result = drawStyle() + drawMenu();
		
		if (request.getParameter("lastname") != null) {
			result += "<p>lastname: <b>" + request.getParameter("lastname") + "</b></p>";
		}
		
		if (request.getParameter("firstname") != null) {
			result += "<p>firstname: <b>" + request.getParameter("firstname") + "</b></p>";
		}
		
		return result;
	}
	
	@RequestMapping("/about")
	public String about(HttpServletRequest request, HttpServletResponse response) {
		String result = drawStyle() + drawMenu();
		result += "<p>About Page</p>";
		
		return result;
	}

	@RequestMapping("/news")
	public String news(HttpServletRequest request, HttpServletResponse response) {
		String result = drawStyle() + drawMenu();
		result += "<p>News Page</p>";
		
		return result;
	}
	
	@RequestMapping("/form")
	public String form(HttpServletRequest request, HttpServletResponse response) {
		String result = drawStyle() + drawMenu();
		result +=
				"<form method=post action='form-controller'>" +
				"<input type=text name=fio>" +
				"<input type=submit value='OK'>" +
				"</form>";
		
		return result;
	}
	
	@RequestMapping("/form-controller")
	public String formController(HttpServletRequest request, HttpServletResponse response) {
		String result = drawStyle() + drawMenu();
//		result += request.getMethod();
		result += request.getParameter("fio");
		
		return result;
	}

}
